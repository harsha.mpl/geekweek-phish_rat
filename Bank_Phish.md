GET /c2_v3/_search
{
  "size": 100,
  "query": {
    "regexp":{
       "panel_url.keyword": {
         "value": ".*(atb|bmo|cibc|desj|desjardins|hsbc|laurentian|lloydsbank|manulife|meridian|mbna|nationalbank|bnc|rbc|rbc2|\/rbs|\/sco|scotia|scoatiabank|simpli|simplii|tangerine|\/td|wellsfargo|test)($|\/.*)"
       }
    }
  }
}