GET /c2_v3/_search
{
  "size": 100,
  "query": {
    "regexp":{
       "panel_url.keyword": {
         "value": "^(http(s)?(:\/\/))?(www.)[a-zA-Z0-9-_.\/](accweb.mouv.desjardins.com|banco.brasil.com.br|chaseonline.chase.com|chaseonline.com|easyweb.td.com|hellobank.fr|login.bankofamerica.com|m.wellsfargo.com|rbcroyalbank.com|secure.bankofamerica.com|secure.ingdirect.fr|secure.tangerine.ca|tdbank.com|wellsfargo.com|www.atbonline.com|www.bmo.com|www.cibc.com|www.cibc.mobi|www.credit|agricole.fr|www.rbs.co.uk|www.scotiaonline.scotiabank.com|www.security.hsbc.co.uk|www1.royalbank.com|www2.soctiaonline.scotiabank.com)($|\/.*)"
       }
    }
  }
}